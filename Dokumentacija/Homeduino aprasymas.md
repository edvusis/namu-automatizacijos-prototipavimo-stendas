# Homeduino aprašymas

Čia pateikta visa reikalinga informacija, reikalinga mikrovaldiklio programavimui išlaikant `Homeduino` integraciją su `Pimatic`

## Veikimo principas

`Homeduino` komunikuoja su `Pimatic` per užregistruotas komandas:

*  `DR {pin}`: Išsiunčia `{pin}` kontakto lygį;
*  `DW {pin} {0/1}`: Pakeičia mikrovaldiklio kontakto `{pin}` lygį į `{0/1}`;
*  `DHT 11 {pin}`: Nuskaito `DHT11` jutiklio, prijungto prie `{pin}` kontakto, informaciją ir išsiunčia `Pimatic`;
*  `AR {pin}`: Išsiunčia `{pin}` kontakto analoginį signalą `Pimatic`;
*  `AW {pin} {0-255}`: Pakeičia mikrovaldiklio analoginio kontakto reikšmę į nurodytą;

Norint išlaikyti `Homeduino` integraciją su `Pimatic`, būtina `loop()` funkcijoje palikti šias funkcijas: 

*  `sCmd.readSerial()`;
*  `rfcontrol_loop()`;

Šios funkcijos reikalingos sėkmingam komandų nuskaitymui ir RF433 įrenginių valdymui. Po jomis galima rašyti savo aplikaciją. Taip pat galima redaguoti užregistruotas kontaktų nuskaitymo ir rašymo funkcijas `digital_read_command`, `digital_write_command`, `analog_read_command` ir `analog_write_command` taip, kad jos atliktų kitus veiksmus, nenumatytus `Homeduino` programinėje įrangoje. __Svarbu__ išlaikyti bendrą komunikacijos formatą, t.y.: atlikus kiekvieną komandą - mikrovaldiklis turi nusiųsit komandą `ACK` su atitinkamais parametrais.

Žemiau pateikta sekų diagrama, kurioje pateikti dažniausiai naudojamų komandų komunikacijos seka. Visos modifikuojamos komandos turi atitikti šį bendravimo protokolą

![alt text](nuotraukos/Homeduino sequence.jpg)


Atsakymai, kuriuos turi išsiųsti `Homeduino`, kai sėkmingai įvykdo komandą:

*  `DR {pin}`: `ACK {pin} {0/1}`;
*  `DW {pin} {0/1}`: `ACK {pin}, {0/1}`;
*  `DHT 11 {pin}`: `ACK {temperature} {humidity}`;
*  `AR {pin}`: `ACK {pin}, {0-255}`;
*  `AW {pin} {0-255}`: `ACK {pin} {0-255}`;