# Sistemos paruošimas naudojimui

Pirmas namų automatizacijos sistemos konfigūravimo žingsnis – išimti įdėtą SD kortelę ir prijungus
ją prie kompiuterio – pakeisti `/etc/wpa_supplicant/wpa_supplicant.conf` failo turinį į:
```
network={
    ssid="SSID"
    psk="PASSWORD"
    scan_ssid=1
}
```

`etc/wpa_supplicant/wpa_supplicant.conf` faile vietoj SSID reikia įrašyti WiFi prieigos taško, prie
kurio norima, kad sistema prisijungtų, SSID, o vietoje PASSWORD – šio prieigos taško slaptažodį.

Suvedus reikiamą informaciją, SD kortelę įdėti atgali į Raspberry Pi mikrokompiuterį ir įjungti jį į
elektros lizdą su maitinimo šaltiniu, tenkinančiu šiuos reikalavimus:

*  Maitinimo jungties tipas: apvali 2.1mm DC jungtis, kurios vidinis kontaktas yra teigiamas;
*  Maitinimo šaltinio darbinė įtampa: 9V

Įjungus namų automatizacijos prototipavimo sistemą – jos ekrane atsiras tekstas, rodantis, kad
įrenginys kraunasi. 

Kai krovimosi procesas bus baigtas – ekrane atsiras įrenginio IP adresas tinkle,
prie kurio jam buvo nurodyta prisijungti wpa_supplicant.conf faile.
