# Pimatic diegimo vadovas

Sukonfigūravus sistemą pagal instrukciją, nurodytą `Sistemos paruosimas.md` faile prie `Raspberry Pi` prisijungiama su bet kokia terminaline programa. 

## Prisijungimas naudojant Linux OS
Linux sistemose galima naudoti įprastą terminalą ir prie įrenginio prisijungti komanda `ssh pi@{ip adresas}` vietoj `{ip adresas}` įvedant `Raspberry Pi` IP adresą, kuris rodomas prie Raspberry Pi prijungtame ekrane.

## Prisijungimas per Windows

Prisijungimui per Windows galima naudoti `Putty` programinę įrangą. Ją galima parsisiųsti čia: https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.73-installer.msi

Putty grafinėje sąsajoje pasirinkti `Connection type: SSH` ir `IP address` laukelyje suvesti `pi@{ip adresas}` vietoj `{ip adresas}` įvedant `Raspberry Pi` IP adresą, kuris rodomas prie Raspberry Pi prijungtame ekrane.

`Port` laukelyje įrašyti `22` ir paspausti mygtuką `Open`

## Pimatic įrašymas

Prisijungus prie Raspberry Pi suvesti jo slaptažodį `raspberry`

Po sėkmingo prisijungimo, norint įrašyti Pimatic programinę įrangą, reikia suvesti šias komandas:

Node.js įrašymas:

```
wget https://nodejs.org/dist/v4.6.2/node-v4.6.2-linux-armv6l.tar.gz -P /tmp
cd /usr/local
sudo tar xzvf /tmp/node-v4.6.2-linux-armv6l.tar.gz --strip=1
```

Pimatic įrašymas:

```
sudo apt-get install build-essential git
cd /home/pi
mkdir pimatic-app
npm install pimatic --prefix pimatic-app --production
```

Nukopijuojame standartinį konfigūracijos failą:

```
cd pimatic-app

cp ./node_modules/pimatic/config_default.json ./config.json
```

Atsidarius `config.json` failą:

`nano config.json`

Susirasti objektą `users` ir jo parametro `password` reikšmę pakeisti į norimą slaptažodį, naudojamą prisijungimui prie Pimatic grafinės sąsajos.

Pirmasis Pimatic paleidimas:

`sudo node_modules/pimatic/pimatic.js start`

Suvedus šią komandą, Pimatic serveris visada pasileis, kai bus įjungiamas įrenginys.

Atlikus visą konfigūraciją - naršyklės laukelyje suvedus Raspberry Pi IP adresą - atsidaro Pimatic programinės įrangos prisijungimo langas. Jame suvedus ankščiau pasirinktą saptažodį - prisijungiame prie sistemos.

Konfigūracija baigta.