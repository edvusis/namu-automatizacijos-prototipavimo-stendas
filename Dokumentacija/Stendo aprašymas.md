# Stendo Aprašymas

Čia pateikta esminė informacija apie namų automatizacijos prototipavimo sistemos techninį stendą. 

## Mygtukai

Stende įmontuoti keturi mygtukai, kurie, kai nuspausti, išsiunčia aukštą loginį signalą į nurodytą valdiklio kontaktą:

![alt text](nuotraukos/mygtukai.png)

Nuotraukoje matoma, kad mygtukai prijungti prie mikrovaldiklio 5, 18, 19, 21 kontaktų. Šie kontaktai gali būti naudojami `HomeduinoContactSensor` įrenginiams Pimatic sistemoje.

## Relės

Stende įmontuotos keturios elektromagnetinės relės, prijungtos prie mikrovaldiklio kontaktų:

![alt text](nuotraukos/reles.png)

Nuotraukoje matoma, kad mygtukai prijungti prie mikrovaldiklio 32, 33, 25, 26 kontaktų. Šie kontaktai gali būti naudojami `HomeduinoSwitch` įrenginiams Pimatic sistemoje.

## DHT11 temperatūros ir drėgmės jutiklis

Stende įmontuotas DHT11 temperatūros ir drėgmės jutiklis, kuris prijungtas prie mikrovaldiklio 27-o kontakto. Šis kontaktas gali būti naudojamas `HomeduinoDHTSensor` įreiginiui Pimatic sistemoje.

Pastaba: naudojant šį įrenginį, būtina grafinės sąsajos `Type` laukelyje nurodyti DHT jutiklio tipą (11), o duomenis galima skaityti intervalu, ne mažesniu, nei 2000ms.

## OLED Ekranas

Prie Raspberry Pi mikrokompiuterio prijungtas OLED ekranas, kuris gali būti naudojamas informacijai atvaizduoti. Šiuo atveju jis atvaizduoja savo IP adresą prisijungtame tinkle. Norint pakeisti šio ekrano rodomą informaciją galima naudoti pateiktą `Raspberry Scripts/ipaddr.py` failą kaip pavyzdį.

# Pastebėjimai

ESp32 mikrovaldiklis turi 32 programuojamus GPIO kontaktus. Kontaktai mikrovaldiklyje sunumeruoti nuo G1 iki G32. Visi kontaktai turi ir kitas funkcijas (žiūrėti pateiktoje nuotraukoje):

![alt text](nuotraukos/esp32_pinout.jpg)

Šaltinis: https://ae01.alicdn.com/kf/HTB1QJ4DM9zqK1RjSZPcq6zTepXaM/ESP32-Development-Board-WiFi-Bluetooth-Ultra-Low-Power-Consumption-Dual-Cores-ESP-32-ESP-32S-Board.jpg_q50.jpg

