# Namu automatizacijos prototipavimo stendas

## Struktūra

`homeduino` - Homeduino sistemos programinis kodas

`MCU Tests` - testavimo failai, skirti ESP32 ir prie jo prijungtų įrenginių testavimui

`Laboratoriniai darbai` - aplankas su laboratorinių darbų aprašais

`Dokumentacija` - dokumentacija su reikalinga informacija pirmam sistemos konfigūravimui ir paleidimui

