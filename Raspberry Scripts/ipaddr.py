# Small init script required to print IP address of RPI when connected to AP

import socket
import time
import I2C_LCD_driver

def get_ip():
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.connect(("8.8.8.8", 80))
  ip_addr = s.getsockname()[0]
  s.close()
  return ip_addr

def main():
  # Setup display
    mylcd = I2C_LCD_driver.lcd()
    ip_addr = get_ip()
    if ip_addr:
        mylcd.lcd_display_string(ip_addr, 1)

if __name__ == '__main__':
    main()
