#include <Arduino.h>
#include "DHT.h"

#define BTN_1 21
#define BTN_2 19
#define BTN_3 18
#define BTN_4 5

#define RELAY 25
#define DHT_PIN 27

void button_test();
void relay_test();
void dht_test();
DHT dht;

void setup() {
  Serial.begin(9800);
  dht.setup(DHT_PIN);
  // put your setup code here, to run once:
}

void loop() {
  // button_test();
  // relay_test();
  dht_test();
}

void button_test(){
  int btn_array[] = {BTN_1, BTN_2, BTN_3, BTN_4};
  int states[] = {0, 0, 0, 0};
  pinMode(btn_array[0], INPUT);
  pinMode(btn_array[1], INPUT);
  pinMode(btn_array[2], INPUT);
  pinMode(btn_array[3], INPUT);

  while(true){
    for(int i = 0; i < 4; i++){
      states[i] = digitalRead(btn_array[i]);
    }
    Serial.printf("Button States: %d %d %d %d\n", states[0], states[1], states[2], states[3]);
    delay(1000);
  }
}

void relay_test(){
  pinMode(RELAY, OUTPUT);
  while(true){
    digitalWrite(RELAY, 1);
    delay(1000);
    digitalWrite(RELAY,0);
    delay(1000);
  }
}

void dht_test(){
 while(true){
   float temp = dht.getTemperature();
   float humid = dht.getHumidity();
   Serial.printf("Temperature: %.2f Humidity: %.2f\n", temp, humid);
   delay(3000);
 }
}